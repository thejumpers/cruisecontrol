#ifndef CAR_H
#define CAR_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
#include "util.h"
#include "macro.h"

class Car
{
    public:
        SDL_Rect body;
        SDL_Texture *tex;
        int speed;
        int newSpeed;
        int acc;
        bool onScreen, laneTransition;
        unsigned int lane;
        unsigned int xChangeLane;

        bool changeSpeed,autoUpdate,updateSpeed;


        Car(int x,int lane, int speed, std::string tex_path);

        virtual ~Car();
        void moveRoad(SDL_Rect* r1, SDL_Rect* r2);
        bool getLaneTransition();
        void setLaneTransition(bool lt);
        void changeLane(bool direction);
        void Draw(void);
        void setSpeed(int s);
        void setPos(int x, int y);
        bool getUpdateMode();
        void setUpdateMode(bool updateMode);
        int getSpeed();
        unsigned int getLane();
        int getPos();
        void operator=(const Car& c);
        void carUpdate(int x, int lane, int speed);
    protected:
    private:
};

#endif // CAR_H

#ifndef UTIL_H_INCLUDED
#define UTIL_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
#include <iostream>
#include <SDL2/SDL_ttf.h>

#include "macro.h"

using namespace std;

extern SDL_Window* win;
extern SDL_Renderer* rend;

extern unsigned long lastGenTime;
extern unsigned int randTime;

void init();
void initRect(SDL_Rect* rect, int x, int y, int h, int w);
TTF_Font *initTTF();
//void initTTF(TTF_Font *font);
//void displayText(char *buffer, SDL_Surface *msg, SDL_Texture *msgTex, SDL_Renderer *r, TTF_Font *font);
int isPressed(SDL_Rect *rect, SDL_Event event);
void cleanup();
void ApplyTex(int ,int,SDL_Texture*,SDL_Rect*,int ,int );
SDL_Texture *LoadTex(string);
void config_scene(SDL_Rect *, SDL_Rect *);
void moveRoad(SDL_Rect*, SDL_Rect*);
//returns true after a random time has passed since last call
bool waitRandTime();
#endif // UTIL_H_INCLUDED

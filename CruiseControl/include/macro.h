#ifndef MACRO_H_INCLUDED
#define MACRO_H_INCLUDED

#define MAXBUTTONS  4
#define MAX_CARS    3
#define MAX_FIELDS  3

#define CAR_TEX     4

#define MAX_SPEED   3
#define MIN_SPEED   1

#define LANE1       1
#define LANE2       2
#define LANE3       3

#define NO_EVENT            0
#define MOUSE_DOWN          1
#define MOUSE_MOTION        2

#define UPDATE_MAIN_CAR     1
#define UPDATE_SECONDARY    3
#define CREATE_CAR          2

#define WINDOW_W 800
#define WINDOW_H 600

#define MENU_X 100
#define MENU_Y 100

#define BTN_X (MENU_X + 60)
#define BTN_Y (MENU_Y + 60)

#define BTN_H (WINDOW_H / (1.5 * 5))
#define BTN_W (WINDOW_W / 1.5 - 120)



#endif // MACRO_H_INCLUDED

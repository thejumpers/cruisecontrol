#ifndef SECONDARYMENU_H
#define SECONDARYMENU_H

#include "util.h"
#include "macro.h"

class SecondaryMenu
{
    public:
        int noFields;
        char* fieldNames[MAX_FIELDS];
        SDL_Texture *mNames[MAX_FIELDS];
        SDL_Texture *mValues[MAX_FIELDS];
        char *fieldValues[MAX_FIELDS];

        SDL_Rect rFieldTitles[MAX_FIELDS];
        SDL_Rect rEditableFields[MAX_FIELDS];

        SDL_Rect bApply;
        SDL_Rect bBack;

        SDL_Texture *mApply, *mBack;

        SDL_Texture *fieldTex;
        SDL_Texture *messageTex;

        SDL_Surface *message;

        TTF_Font *font;
        SDL_Color textColor = { 0x60, 0x60, 0x60 };

        SecondaryMenu(char* fields[], int no_fields, SDL_Texture *fieldTex, TTF_Font *font);
        virtual ~SecondaryMenu();

        void drawMenu();
        void updateFields(int downArr, char *text);

    protected:
    private:
};

#endif // SECONDARYMENU_H


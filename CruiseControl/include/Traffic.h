#ifndef TRAFFIC_H
#define TRAFFIC_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
#include <iostream>

#include "Car.h"
#include "util.h"



using namespace std;

class Traffic {
    public:
        unsigned int carNo;
        unsigned int contor;
        int speed;
        int speedAux;
        unsigned int laneAux, carIndex;

        int changeID;
        // add to distance between cars
        int deltaX;

        // The bad-boy cars
        Car *carArr[MAX_CARS];
        Car *car;
        // this holds the textures
        string carTextures[CAR_TEX] = {"./res/top-cenusiu.bmp",
                                            "./res/top-verzui.bmp",
                                            "./res/top-albastrui.bmp",
                                            "./res/top.bmp"}; // last one is useless ?

        Traffic();
        virtual ~Traffic();
        void moveRoad(SDL_Rect* r1, SDL_Rect* r2);
        void avoidOverlay(int,int);
        void initTraffic();
        void updateAtCollision(Car *car1, Car *car2);
        bool checkCollision(Car *car1, Car *car2);
		bool checkSafeTransition(Car *c, unsigned int nextLane);
        void trafficControlTop(Car *c);
        void trafficControl(char *fieldValues[]);
        void updateFromInterface(int updateID, char *fieldValues[]);

};

#endif // UTIL_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <string>
#include <iostream>
#include "util.h"
#include "Car.h"
#include "Traffic.h"
#include "SecondaryMenu.h"
#include "macro.h"

SDL_Color textColor = { 0xFF, 0xFF, 0xFF };

using namespace std;


/***
*Changes:
*
*menu closes when you apply new settings
*added reference distance for ACC car
*added new car functions
*added update car functions (change lane)
*moved Init_TTF() to util.h
*moved MACROS to new file - macro.h
*replaced all .png files with .bmp to run on Udoo
***/

unsigned int randTime;
unsigned long lastGenTime;

// create window
SDL_Window *win;
// used to draw all the things on screen
SDL_Renderer* rend;

//TODO : use this for cruise control alg
//TODO: if feel like it, move this to a class too
//int speed = -5;

SDL_Rect menuBase;
SDL_Texture* menuBaseTex;

SecondaryMenu *secMenus[3];

/// move to util.cpp

SDL_Rect menuButtons[MAXBUTTONS];

SDL_Texture *button_press;
SDL_Texture *button_unpress;

/* variables for text display on buttons */

TTF_Font *font;
SDL_Texture* messageTex[MAXBUTTONS];

SDL_Surface *text = NULL;
SDL_Surface *message;

int menuID = 1;


void defaultRunMode(Traffic *traffic, SDL_Rect road1,SDL_Rect road2,SDL_Texture* road_tex)
{
    SDL_SetRenderDrawColor(rend, 0,128,0,255);
    SDL_RenderClear(rend);

    ApplyTex(road1.x,road1.y,road_tex,NULL, WINDOW_W, WINDOW_H / 2);  //modifica aici 800 si 300
    ApplyTex(road2.x,road2.y,road_tex,NULL, WINDOW_W, WINDOW_H / 2);

    traffic->car->Draw();
    traffic->trafficControl(secMenus[menuID - 2]->fieldValues);

    SDL_RenderPresent(rend);
}

bool selectMenu(SDL_Event event)
{
    bool showMenu = true;


     if(menuID == 1)
     {
        for(int i = 0; i < MAXBUTTONS; i++)
        {
            //message = TTF_RenderText_Solid( font, buttonLabel[i], textColor );
            //messageTex = SDL_CreateTextureFromSurface(rend, message);
            int ret = isPressed(&menuButtons[i], event);
            if( ret == MOUSE_MOTION)
            {
                ApplyTex(menuButtons[i].x,menuButtons[i].y, button_press, NULL,menuButtons[i].w,menuButtons[i].h);
            }
            else
            {
                if(ret == MOUSE_DOWN && i != 3) {
                    menuID = i + 2;
                    SDL_Delay(100);
                    //break;
                    showMenu = true;
                    }

                ApplyTex(menuButtons[i].x,menuButtons[i].y, button_unpress, NULL,menuButtons[i].w,menuButtons[i].h);
            }

            if(ret == MOUSE_DOWN && i == 3)
            {
                showMenu = false;
            }

            ApplyTex(menuButtons[i].x + 30,menuButtons[i].y + 10, messageTex[i], NULL,menuButtons[i].w - 60,menuButtons[i].h - 20);
        }
    } else {
    /// MenuId!= 1
        secMenus[menuID - 2]->drawMenu();
    }

    return showMenu;
}

bool menuMode(SDL_Event event, Traffic* traffic)
{
    bool showMenu;
    ApplyTex(menuBase.x,menuBase.y, menuBaseTex, NULL,menuBase.w,menuBase.h);

    if(menuID > 1 && isPressed(&secMenus[menuID - 2]->bBack, event) == MOUSE_DOWN) {
        SDL_Delay(100);
        menuID = 1;
        return true;

    }

    if(menuID > 1 && isPressed(&secMenus[menuID - 2]->bApply, event) == MOUSE_DOWN) {
        traffic->updateFromInterface(menuID - 1, secMenus[menuID - 2]->fieldValues);
        menuID = 1;
        SDL_Delay(100);
    }

    showMenu = selectMenu(event);

    SDL_RenderPresent(rend);
    return showMenu;
}


int main()
{

    Traffic *traffic;
    bool run = true;
    SDL_Event event;
    SDL_Texture *road_tex;
    SDL_Rect road1,road2;

    int downArr = 0;
    bool showMenu = false;

    // First Sec.menu: speedText distInput
    char* fields[][3] = {{"Set Speed", "Set Distance", ""},
                        {"Set Speed", "Set Lane", "Set Color"},
                        {"Set Speed", "Set Lane", "Set Color"}};
    int dim[] = {2, 3, 3};

    static const char* buttonLabel[] = {"Configure target car","Add new cars","Update existing cars", "       Exit       "};


    randTime = 500 + (rand() % 999); // rand_tine results between 500ms and 1499 ms

    init();
    font = initTTF();
    traffic = new Traffic();
    config_scene(&road1,&road2);
    road_tex = LoadTex("./res/road.bmp");
    initRect(&menuBase, MENU_X, MENU_Y, WINDOW_H / 1.5 + 20, WINDOW_W / 1.5);

    for(int i = 0; i < MAXBUTTONS; i++) {
        initRect(&menuButtons[i], BTN_X, MENU_Y + 50 + i  * (BTN_H + 10), BTN_H, BTN_W);
    }

    SDL_Texture *fieldTex = LoadTex("./res/PressedButton.bmp");

    for(int i = 0; i < 3; i++) {
        secMenus[i] = new SecondaryMenu(fields[i], dim[i], fieldTex, font);
    }

    button_press= LoadTex("./res/PressedButton.bmp");
    button_unpress = LoadTex("./res/BlueSquareButton.bmp");
    menuBaseTex = LoadTex("./res/menu2.bmp");


    for(int i = 0; i < MAXBUTTONS; i++)
    {
        message = TTF_RenderText_Solid( font, buttonLabel[i], textColor );
        messageTex[i] = SDL_CreateTextureFromSurface(rend, message);
    }


    while(run)
    {
        ///EVENTS
        while(SDL_PollEvent(&event))
        {

            if(event.type == SDL_QUIT)
            {
                run = false;
            }
            else if(event.type == SDL_KEYDOWN)
            {

                switch(event.key.keysym.scancode)
                {
                    case SDL_SCANCODE_ESCAPE:
                        run = false;
                        break;

                    case SDL_SCANCODE_M:
                        if(showMenu)
                            showMenu = false;
                        else
                            showMenu = true;
                        break;
                    case SDL_SCANCODE_DOWN:
                        {
                        downArr ++;
                        downArr = downArr % secMenus[menuID - 2]->noFields;
                        break;
                        }

                    case SDL_SCANCODE_UP:
                        {
                            downArr --;
                            if(downArr < 0) downArr = -downArr;
                                downArr = downArr % secMenus[menuID - 2]->noFields;
                        break;

                       }
                    default:
                        break;
                }
            }

            else if(event.type == SDL_TEXTINPUT && (menuID != 1))
            {
                char *cifra = event.text.text;
                if((*cifra - '0') >= 0 && (*cifra - '0') <= 9)
                {

                    secMenus[menuID - 2]->updateFields(downArr, event.text.text);

                }

            }
        }

        ///LOGICS
        traffic->moveRoad(&road1, &road2);

        ///RENDER

        if(showMenu)
       {
           showMenu = menuMode(event, traffic);
       }
       else
       {
            defaultRunMode(traffic,road1,road2,road_tex);
       }
    }
    cleanup();

    return 0;
}

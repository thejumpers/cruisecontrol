#include "util.h"

void init(){

    win = SDL_CreateWindow("Automatic Cruis Control Simulator", 0, 0,WINDOW_W, WINDOW_H, SDL_WINDOW_SHOWN);
    rend = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_TARGETTEXTURE);
}

void initRect(SDL_Rect* rect, int x, int y, int h, int w)
{
    rect->x = x;
    rect->y = y;
    rect->h = h;
    rect->w = w;
}

/*
void displayText(char *buffer, SDL_Surface *msg, SDL_Texture *msgTex, SDL_Renderer *r, TTF_Font *font)
{
    SDL_Surface *m;
    SDL_Texture *t;

    SDL_Color textColor = { 0xFF, 0xFF, 0xFF };
    m = TTF_RenderText_Solid( font, buffer, textColor );
    t = SDL_CreateTextureFromSurface(r, m);

    //*msg = *m;
    //*msgTex = *t;
}
*/

TTF_Font *initTTF()
{
    TTF_Font *font = NULL;

    //use ttf fonts
    if(TTF_Init() == -1)
        cout<<"Error initializating TTF !"<<endl;
    else cout<<"... TFF initializated "<<endl;

    //load font

    if(font = TTF_OpenFont( "./res/Roboto-Light.ttf", 100 )) {
        cout<< "...font loaded "<<endl;
    }
    else {
        cout<<"Error loading font !"<<endl;
    }
    return font;
}

int isPressed(SDL_Rect *rect, SDL_Event event)
{
    int x = 0, y = 0;

    if( event.type == SDL_MOUSEMOTION || event.type == SDL_MOUSEBUTTONDOWN)
    {
        x = event.motion.x;
        y = event.motion.y;

        if( ( x > rect->x ) && ( x < rect->x + rect->w ) && ( y > rect->y ) && ( y < rect->y + rect->h ) ) {

            if(event.type == SDL_MOUSEMOTION)
                return MOUSE_MOTION;
            return MOUSE_DOWN;
        }

    }

    return NO_EVENT;
}

void cleanup(){

    // Uncomment after we moved all we had related to text, surface and buttons here...
    /*
    SDL_FreeSurface( message );

    //Close the font that was used
    TTF_CloseFont( font );

    //Quit SDL_ttf
    TTF_Quit();
    */

    SDL_DestroyWindow(win);
    SDL_DestroyRenderer(rend);
}


void ApplyTex(int x,int y,SDL_Texture *tex,SDL_Rect *clip ,int w,int h ){

    SDL_Rect pos;

    pos.x = x;
    pos.y = y;

    if( clip != NULL )
    {
        pos.w = clip->w;
        pos.h = clip->h;
    }
    else
    SDL_QueryTexture( tex,NULL,NULL,&pos.w,&pos.h );

    if( w != -1 ) pos.w = w;
    if( h != -1 ) pos.h = h;

    SDL_RenderCopy(rend,tex,clip,&pos );
}

SDL_Texture *LoadTex(string file){

    SDL_Texture *loadedtex = NULL;

    loadedtex = IMG_LoadTexture(rend,file.c_str() );

    return loadedtex;
}

void config_scene(SDL_Rect *r1, SDL_Rect *r2)
{
//set the initial position for the roads
    r1->w = WINDOW_W;
    r1->h = WINDOW_H/2;
    r1->x = 0;
    r1->y = WINDOW_H/2 - r1->h/2;

    r2->w = WINDOW_W;
    r2->h = WINDOW_H/2;
    r2->x = r1->x + WINDOW_W;
    r2->y = r1->y;
}

bool waitRandTime()
{
        bool elapsed = false;

        if(SDL_GetTicks() - lastGenTime >= randTime)
        {
            elapsed = true;
            randTime = ((rand() % 3)*1000) + (rand() % 666); // rand_tine results between 500ms and 1499 ms
            lastGenTime = SDL_GetTicks();
        }
        if(elapsed)
        {
            return true;
        }
    return false;
}


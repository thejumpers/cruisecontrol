#include "Car.h"

Car::Car(int x = 800, int lane = 1, int speed = 1, string tex_path = NULL)
{
    this->body.x = x;
    if(lane == 1) this->body.y = 180;
    else if(lane == 2) this->body.y = 265;
    else if(lane == 3) this->body.y = 350;
    this->lane = lane;
    this->body.h = 45;
    this->body.w = 85;
    this->speed = speed;

    this->tex = LoadTex(tex_path);
    this->onScreen = true;
    this->autoUpdate = true;
    this->laneTransition = false;
    this->changeSpeed = false;
    this->updateSpeed = false;

    this->xChangeLane = (int) (20 + rand() % WINDOW_W);
}

Car::~Car()
{
    //dtor
}

void Car::changeLane(bool direction)
{
    switch(this->lane)
{
    case 1:
    {
       if(this->body.y == 265)
      {
        this->lane = 2;
        this->laneTransition = false;
        if(changeSpeed)
            this->speed = (1 + int(rand() % 3));
      }
      else
      {
        this->body.y += 1;
      }
    break;
    }

    case 2:
    {
        if(direction)
        {
            if(this->body.y == 180)
              {
                this->lane = 1;
                this->laneTransition = false;
                if(changeSpeed)
                    this->speed = (1 + int(rand() % 3));
              }
              else
              {
                this->body.y -= 1;
              }
        }
        else
        {
            if(this->body.y == 350)
              {
                this->lane = 1;
                this->laneTransition = false;
                if(changeSpeed)
                    this->speed = (1 + int(rand() % 3));
              }
              else
              {
                this->body.y += 1;
              }
        }
        break;
    }

    case 3:
    {
      if(this->body.y == 265)
      {
        this->lane = 2;
        this->laneTransition = false;
        if(changeSpeed)
            this->speed = (1 + int(rand() % 3));
      }
      else
      {
        this->body.y -= 1;
      }
      break;
    }
}

}

void Car::Draw()
{
    if(this->updateSpeed)
    {
        this->setSpeed(this->newSpeed);
    }
// change car position depending on speed
    if(this->body.x > -85)
    {
        this->body.x -= this->speed;

        ApplyTex(this->body.x,this->body.y,this->tex,NULL,85,45);
        if(this->laneTransition)
        {
            this->changeLane(true);
        }
    }
    else
    {
        this->onScreen = false;
    }
}


void Car::setSpeed(int s)
{
    // slow down
    this->newSpeed = s;
    this->acc = (10 - this->speed) / 3;
    if(this->speed < this->newSpeed)
        {
            this->speed += this->acc;
        }
        else
            this->updateSpeed = false;
    // if you go over the wanted speed, slow down immedeatly
    if(this->speed > this->newSpeed)
        {
        this->speed = this->newSpeed;
        }
}

void Car::setPos(int x, int y)
{
    this->body.x = x;
    this->body.y = y;
}

int Car::getSpeed() { return this->speed; }

bool Car::getUpdateMode() { return this->autoUpdate; }

void Car::setUpdateMode(bool updateMode) { this->autoUpdate = updateMode ;}

unsigned int Car::getLane() { return this->lane; }

bool Car::getLaneTransition() { return laneTransition; }

void Car::setLaneTransition(bool lt) { this->laneTransition = lt; }

int Car::getPos() { return this->body.x; }

void Car::operator=(const Car& c)
{
    speed = c.speed;
    body = c.body;
    tex = c.tex;
    onScreen = c.onScreen;
}

void Car::carUpdate(int x, int lane, int speed)
{
    int vertical;
    if(lane == 1) vertical = 185;
    else if(lane == 2) vertical = 265;
    else if(lane == 3) vertical = 350;
    this->lane = lane;
    this->setPos(x,vertical);
    this->setSpeed(speed);
    this->onScreen = true;
    this->laneTransition = false;
    this->xChangeLane = (int) (20 + rand() % WINDOW_W);
}


#include "SecondaryMenu.h"

SecondaryMenu::SecondaryMenu(char* fields[], int noFields, SDL_Texture *fieldTex, TTF_Font *font)
{
    //ctor
    this->noFields = noFields;
    for(int i = 0; i < this->noFields; i++) {
         this->fieldNames[i] = fields[i];

        message = TTF_RenderText_Solid( font, fieldNames[i], textColor );
        mNames[i] = SDL_CreateTextureFromSurface(rend, message);

        initRect(&this->rEditableFields[i], BTN_X + 200, MENU_Y + (i+1) * (BTN_H + 10), BTN_H - 20, BTN_W / 2);
        initRect(&this->rFieldTitles[i], BTN_X , MENU_Y + (i+1) * (BTN_H + 10), BTN_H, BTN_W / 2);

        fieldValues[i] = (char *) malloc(sizeof(char) * 20);
        strcpy(fieldValues[i], "");

        message = TTF_RenderText_Solid( font, fieldValues[i], textColor );
        mValues[i] = SDL_CreateTextureFromSurface(rend, message);
    }

    initRect(&this->bBack, BTN_X + 250, MENU_Y + 3 * (BTN_H + 10) + 70, BTN_H, BTN_W / 2);
    initRect(&this->bApply, BTN_X - 50, MENU_Y + 3 * (BTN_H + 10) + 70, BTN_H, BTN_W / 2);

    this->fieldTex = fieldTex;
    this->font = font;

    message = TTF_RenderText_Solid( font, "   Back   ", textColor );
    mBack = SDL_CreateTextureFromSurface(rend, message);

    message = TTF_RenderText_Solid( font, "   Apply   ", textColor );
    mApply = SDL_CreateTextureFromSurface(rend, message);


}

SecondaryMenu::~SecondaryMenu()
{
    //dtor
}

void SecondaryMenu::drawMenu()
{

    for(int i = 0; i < this->noFields; i++) {
        ApplyTex(rEditableFields[i].x, rEditableFields[i].y,
            fieldTex, NULL, rEditableFields[i].w, rEditableFields[i].h);


        //message = TTF_RenderText_Solid( font, fieldValues[i], textColor );
        //messageTex = SDL_CreateTextureFromSurface(rend, message);

        ApplyTex(rEditableFields[i].x + 70, rEditableFields[i].y, mValues[i], NULL,
            rEditableFields[i].w - 140, rEditableFields[i].h);

        //message = TTF_RenderText_Solid( font, fieldNames[i], textColor );
        //messageTex = SDL_CreateTextureFromSurface(rend, message);

        ApplyTex(rFieldTitles[i].x + 40, rFieldTitles[i].y +10 , mNames[i], NULL,
            rFieldTitles[i].w - 60, rFieldTitles[i].h - 20);
    }

    //message = TTF_RenderText_Solid( font, "   Back   ", textColor );
    //messageTex = SDL_CreateTextureFromSurface(rend, message);

    ApplyTex(bBack.x,bBack.y, fieldTex, NULL,bBack.w, bBack.h);
    ApplyTex(bBack.x,bBack.y, mBack, NULL,bBack.w,bBack.h);

    //message = TTF_RenderText_Solid( font, "   Apply   ", textColor );
    //messageTex = SDL_CreateTextureFromSurface(rend, message);

    ApplyTex(bApply.x,bApply.y, fieldTex, NULL,bBack.w, bBack.h);
    ApplyTex(bApply.x,bApply.y, mApply, NULL,bApply.w,bApply.h);

}

//TODO: downArr will not be bool any more
void SecondaryMenu::updateFields(int downArr, char *text)
{
     strcpy(this->fieldValues[downArr], text);

     message = TTF_RenderText_Solid( font, fieldValues[downArr], textColor );
     mValues[downArr] = SDL_CreateTextureFromSurface(rend, message);

}


#include "Traffic.h"

Traffic::Traffic()
{
	this->initTraffic();
	this->car = new Car(WINDOW_H / 6, 1, 0, "./res/top.bmp");
	this->carNo = 3;
	this->contor = 0;
	this->speed = -5;
	this->deltaX = 0;

	//variables used for user input
	this->laneAux= 3;
	this->speedAux = 3;
	this->carIndex = 0;
}

Traffic::~Traffic()
{

}

void Traffic::moveRoad(SDL_Rect* r1, SDL_Rect* r2)
{

r1->x += speed;
r2->x += speed;

if(r1->x <= -WINDOW_W)
{
    r1->x = r2->x + WINDOW_W;
}
if(r2->x <= -WINDOW_W)
{
    r2->x = r1->x + WINDOW_W;
}
}


void Traffic::avoidOverlay(int carPos, int maxNo)
{
     for(int j = 0; j < maxNo; j++) {
        if(carPos != j && checkCollision(carArr[j], carArr[carPos])) {
            carArr[carPos]->body.x += carArr[carPos]->body.w + int(rand() % 10 + 5);
        }
    }
}

/* Generate the first 3 cars */
void Traffic::initTraffic()
{
    for(int i=0; i<MAX_CARS;i++)
    {
        int lane = 1 + (rand() % 3);
        int speed = (MIN_SPEED + int(rand() % MAX_SPEED));

        carArr[i] = new Car(WINDOW_W,lane,speed,carTextures[i % 3]);
        carArr[i]->changeSpeed = true;

        for(int j = 0; j < i; j++) {
            if(checkCollision(carArr[j], carArr[i])) {
                carArr[i]->body.x += carArr[i]->body.w + int(rand() % 10 + 5);
            }
        }

    }
}

/* In case one car enters in the collision
 * radius of the other car, reset the speed
 * of both cars to the minimum speed of the 2
 */
void Traffic::updateAtCollision(Car *car1, Car *car2)
{
    int minSpeed = min(car1->getSpeed(), car2->getSpeed());

    car1->setSpeed(minSpeed);
    car2->setSpeed(minSpeed);

}

/* Checks collision between 2 cars */
bool Traffic::checkCollision(Car *car1, Car *car2)
{
    int xi, xj;
    const int dx = WINDOW_W / 6 + 20 * this->deltaX;

    // collision posible only if cars are on the same lane
    if(car1->lane == car2->lane)
    {
        xi = car1->getPos();
        xj = car2->getPos();

		// a distance less than dx, means collision
        if( abs(xi-xj) <= dx)
            {
               return true;
            }
    }

   return false;
}

// TODO use parameters for lane !! and 85 ?
/* Check if the lane can safely be changed */
bool Traffic::checkSafeTransition(Car *c, unsigned int nextLane)
{
	/* The future x position of the car
	 * on the changed lane
	 */
    int x1 = c->body.x + c->speed * 85;
    Car *futureC1 = new Car(x1, nextLane, c->speed, "");

    for(int i = 0; i < MAX_CARS; i++) {
		/* Where will be another car on the road,
		 * at that same moment of time
		 */
        int x2 = carArr[i]->body.x + carArr[i]->speed * 85;
        Car *futureC2 = new Car(x2, carArr[i]->lane, carArr[i]->speed, "");

        if(carArr[i] != c && checkCollision(futureC1, futureC2))
            return false;
    }
    return true;
}

// This implements the behaviour
// of the cars about to collide with our car
void Traffic::trafficControlTop(Car *c)
{

        if((c->lane == LANE1 || c->lane == LANE3) && checkSafeTransition(c, LANE2)) {
            c->laneTransition = true;
            contor = 0;

        }

        else if (c->lane == LANE2 && checkSafeTransition(c, LANE1)) {
            c->laneTransition = true;
            contor = 0;

        }
        else
            c->speed = car->speed;

}

// Randomly generate cars with collision detection
// and safely change of lane
void Traffic::trafficControl(char* fieldValues[])
{

    for(int i = 0; i<MAX_CARS; i++)
    {
        if(checkCollision(carArr[i],car)) {
            unsigned int contor_old = contor;
            trafficControlTop(carArr[i]);
            contor = contor_old;
        }

        for(int j = i + 1; j < MAX_CARS; j++)
            {
                // collision posible only if cars are on the same lane
                if(checkCollision(carArr[i], carArr[j]))
                {
                  updateAtCollision(carArr[i], carArr[j]);
                }
            }

            carArr[i]->Draw();
            if(!(carArr[i]->onScreen) && waitRandTime() && carArr[i]->autoUpdate)
                {
                contor++;
                carArr[i]->carUpdate(WINDOW_W,(1 + (rand() % 3)),(MIN_SPEED + int(rand() % MAX_SPEED)));
//                this->avoidOverlay(i, MAX_CARS);
                }
            else if(!(carArr[i]->onScreen) && !(carArr[i]->autoUpdate))
            {
                contor++;
                //unsigned int carIndex = atoi(fieldValues[2]);
                carArr[i]->carUpdate(WINDOW_W,laneAux,speedAux);
                carArr[i]->setUpdateMode(true);
            }

           /* if(contor % 5 == 4 &&
                (carArr[i]->body.x < carArr[i]->xChangeLane)) {

                trafficControlTop(carArr[i]);

                if(contor != 0) {
                    carArr[i]->speed = MIN_SPEED + (int) (rand() % MAX_SPEED);
                }

            }*/


    }
}

void Traffic::updateFromInterface(int updateID, char *fieldValues[])
{
    int speedAux;
    switch(updateID) {
        case UPDATE_MAIN_CAR: {
            if(strcmp(fieldValues[0], "") != 0) {
                this->speed = -atoi(fieldValues[0]);

            }

            if(strcmp(fieldValues[1], "") != 0) {
                // Change lane
                this->deltaX = atoi(fieldValues[1]);
            }

            break;
        }

        case UPDATE_SECONDARY: {
            if(strcmp(fieldValues[2], "") != 0 && atoi(fieldValues[2]) < MAX_CARS) {
                if(strcmp(fieldValues[0], "") != 0)
                {
                    speedAux = atoi(fieldValues[0]);
                    laneAux = atoi(fieldValues[1]);
                    carIndex = atoi(fieldValues[2]);
                    this->carArr[carIndex]->setSpeed(speedAux);
                    if(this->carArr[carIndex]->getLane() != laneAux)
                    {
                        for(int i=0; i< MAX_CARS; i++)
                            if(i != carIndex)
                            {
                                if(!(checkCollision(carArr[carIndex], carArr[i])))
                                {
                                    this->carArr[carIndex]->laneTransition = true;
                                    if(laneAux == 1) // from lane 2 move to lane 1
                                        this->carArr[carIndex]->changeLane(true);
                                    if(laneAux == 3) // from lane 2 move to lane 3
                                        this->carArr[carIndex]->changeLane(false);
                                }
                            }

                    }

                }
            }
            break;
        }
        case CREATE_CAR: {
            if(strcmp(fieldValues[2], "") != 0 && atoi(fieldValues[2]) < MAX_CARS) {
                if(strcmp(fieldValues[0], "") != 0) {
                    this->carIndex =  atoi(fieldValues[2]);
                    this->speedAux = atoi(fieldValues[0]);
                    this->laneAux = atoi(fieldValues[1]);
                    this->carArr[atoi(fieldValues[2])]->setUpdateMode(false);
                }
            }
            break;
        }
    }


}
